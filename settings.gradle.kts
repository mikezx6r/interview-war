pluginManagement {
    repositories {
        maven { setUrl("https://dl.bintray.com/kotlin/kotlin-eap") }
        gradlePluginPortal()
        jcenter()
    }
}

rootProject.name = "interview-war"

