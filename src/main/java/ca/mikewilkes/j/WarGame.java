package ca.mikewilkes.j;

import kotlin.Pair;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class WarGame {

    public static void main(String[] args) {
        Game game = new Game(4, 52);

        game.setup();
        game.play();
        game.showWinner();
    }

    static class Game {
        private List<Player> players = new ArrayList<>();
        private final int deckSize;
        private final int playerCount;

        Game(int playerCount, int deckSize) {
            this.playerCount = playerCount;
            this.deckSize = deckSize;

            for (int i = 0; i < playerCount; ++i) {
                players.add(new Player("Player " + (i + 1)));
            }
        }

        void setup() {
            final List<Integer> deck = IntStream.range(0, deckSize)
                    .limit(deckSize)
                    .boxed()
                    .collect(Collectors.toList());

            Collections.shuffle(deck);

            for (int i = 0; i < deckSize; ++i) {
                players.get(i % playerCount).addCard(deck.get(i));
            }
        }

        void play() {
            while (playersHaveCards()) {
                final List<Pair<Player, Integer>> playerCards = players.stream().map(it -> new Pair<>(it, it.nextCard())).collect(Collectors.toList());

                Pair<Player, Integer> winning = playerCards.stream().max(Comparator.comparing(Pair<Player, Integer>::getSecond)).orElseThrow((Supplier<IllegalStateException>) () -> {
                    throw new RuntimeException("Must be a winner");
                });

                winning.getFirst().score += playerCount;
            }
        }

        void showWinner() {
            players.forEach(Player::printDetails);

            final Map<Integer, List<Player>> winners = players.stream().collect(Collectors.groupingBy(it -> it.score));

            if (winners.size() == 1) {
                System.out.println(playerCount + " way Tie");
            } else {
                final Map.Entry<Integer, List<Player>> playersByScore = winners.entrySet().stream().max(Comparator.comparing(Map.Entry::getKey)).orElseThrow((Supplier<IllegalStateException>) () -> {
                    throw new RuntimeException("Must be a winner");
                });
                playersByScore.getValue().forEach(player -> System.out.println("The winner is " + player.name));
            }
        }

        boolean playersHaveCards() {
            return players.stream().allMatch(Player::hasMoreCards);
        }
    }

    static class Player {
        private List<Integer> cards = new ArrayList<>();
        String name;
        public int score = 0;

        Player(String name) {
            this.name = name;
        }

        public void addCard(Integer card) {
            cards.add(card);
        }

        public boolean hasMoreCards() {
            return !cards.isEmpty();
        }

        public int nextCard() {
            return cards.remove(0);
        }

        public static void printDetails(Player player) {
            System.out.println(player.name + "-" + player.cards + ":" + player.score);
        }
    }
}
