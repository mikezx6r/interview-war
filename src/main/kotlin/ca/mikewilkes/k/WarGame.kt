package ca.mikewilkes.k

fun main(args: Array<String>) {
    val game = Game(4, 52)

    game.setup()
    game.play()
    game.showWinner()
}

class Game(private val playerCount: Int, private val deckSize: Int) {
    private val players = MutableList(playerCount, { Player("Player ${it + 1}") })

    fun setup() {
        (1..deckSize).toList().shuffled().forEachIndexed { index, it ->
                players[index % playerCount].addCard(it)
            }
    }

    fun play() {
        while (playersHaveCards()
        ) {
            val playerCards = players.map { Pair(it, it.nextCard()) }

            val winning = playerCards.maxBy { it.second } ?: throw IllegalStateException("Must be a highest card")

            // winning player gets 1 for each card collected, which will equal # of players in game
            winning.first.score += playerCount
        }
    }

    fun showWinner() {
        players.forEach {
            println("Score for player ${it.name}: ${it.score}")
        }

        val winners = players.groupBy { it.score }
        if (winners.size == 1
        ) {
            println("$playerCount way Tie")
        } else {
            winners.maxBy { e -> e.key }
                ?.value
                ?.forEach {
                    println("The winner is ${it.name}")
                } ?: throw RuntimeException("Must be a winner")
        }
    }

    private fun playersHaveCards(): Boolean = players.all { it.hasMoreCards() }
}


class Player(val name: String) {
    private var cards = mutableListOf<Int>()
    var score: Int = 0

    fun addCard(card: Int) {
        cards.add(card)
    }

    fun hasMoreCards(): Boolean = cards.isNotEmpty()
    fun nextCard(): Int = cards.removeAt(0)
}
