## Overview

This project contains a Kotlin JVM application using the Kotlin Gradle script

It represents a potential solution to the Interview question outlined [here](https://dev.to/mortoray/interview-question-a-two-player-card-game-67i).

##  Two Player Card Game

I introduce the question with a preamble about being focused on the design and structure of the code. I don't need to have a running program, but need to see an entry point and how it would be used.

Here is the description of the game simulation.

- this is a two player card game
- the game starts with a deck of cards
- the cards are dealt out to both players
- on each turn:
    - both players turn over their top-most card
    - the player with the higher valued card takes the cards and puts them in their scoring pile (scoring 1 point per card)
- this continues until the players have no cards left
- the player with the highest score wins

It's considered a simulation because the players don't have any choices to make. We don't need to worry about input.
